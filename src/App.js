import React, {Component} from 'react';
import Student from "./pages/Student";
import {BrowserRouter as Router, Route, Switch} from "react-router-dom";
import Home from "./pages/Home";
import {ToastContainer} from "react-toastify";
import Country from "./pages/Country";
import Region from "./pages/Region";
import District from "./pages/District";

class App extends Component {
    render() {
        return (
               <div className="container pt-3">
                   <ToastContainer/>
                   <Router>
                       <Switch>
                           <Route exact path={"/student"} component={Student}/>
                           <Route exact path={"/country"} component={Country}/>
                           <Route exact path={"/region"} component={Region}/>
                           <Route exact path={"/district"} component={District}/>
                           <Route exact path={"/"} component={Home}/>
                       </Switch>
                   </Router>
               </div>
        );
    }
}

App.propTypes = {};

export default App;