import {BASEURL} from "./constant";

export default {
    //STUDENT
    getStudents:'student/getAllStudents',
    addStudents:'student/saveOrEditStudent',
    editStudents:'student/editStudent',
    deleteStudents:'student/deleteStudent',

    //COUNTRY
    addCountry:'country',
    getCountries:'country',
    editCountry:'country',
    deleteCountry:'country',

    //REGION
    addRegion:'region/saveRegion',
    editRegion:'region/editRegion',
    getRegions:'region',
    deleteRegion:'region/deleteRegion',
    getAllDistrictOfRegion:'region/getAllDistrictOfRegion',

    //DISTRICT
    addDistrict:'district/saveDistrict',
    getDistrict:'district/getAllDistrict',
    editDistrict:'district/editDistrict',
    deleteDistrict:'district/deleteDistrict',
}