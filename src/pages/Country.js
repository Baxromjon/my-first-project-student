import React, {Component} from 'react';
import {Modal, ModalBody, ModalFooter, ModalHeader} from "reactstrap/es";
import {AvField, AvForm} from 'availity-reactstrap-validation'
import request from "../utils/request";
import api from "../utils/api";
import {toast} from "react-toastify";

class Country extends Component {
    state = {
        countries: [],
        currentCountry: '',
        showModal: false,
        showModalDelete: false,
    }
    openModal = () => {
        this.setState({showModal: true})
    }
    closeModal = () => {
        this.setState({showModal: false})
    }

    componentDidMount() {
        this.getCountries();
    }

    getCountries = () => {
        let current = this.state.currentCountry;
        request({
            url: api.getCountries,
            method: 'GET',
        }).then(ans => {
            this.setState({countries: ans.data._embedded.list})
        }).catch(err => {
        })
    }
    saveCountry = (e, v) => {
        console.log(v)
        let current = this.state.currentCountry;
        request({
            url: current ? (api.editCountry + '/' + current.id) : api.addCountry + '',
            method: current ? 'PUT' : 'POST',
            data: v
        }).then(res => {
            this.getCountries();
            this.closeModal();
        }).catch(err => {
        })
    }
    deleteCountry = () => {
        let current = this.state.currentCountry;
        console.log(current)
        request({
            url: api.deleteCountry + '/' + current.id,
            method: 'DELETE',
        }).then(ans => {
            // alert("Student Deleted")
            // toast.success('Deleted')

            this.hideDeleteModal();
            this.getCountries();
            console.log(ans)
        }).catch(err => {
            console.log(err)
        })
    }
    editCountry = (country) => {
        this.openModal();
        this.setState({currentCountry: country})
    }
    deleteModal = (country) => {
        this.setState({
            showModalDelete: true,
            currentCountry: country
        })
    }
    hideDeleteModal = () => {
        this.setState({
            showModalDelete: false,
            currentCountry: '',
        })
    }

    render() {
        return (
            <div>
                <h1 className="text-center">Country List</h1>
                <button className="btn btn-primary"
                        onClick={this.openModal}>+add country
                </button>
                <table className="table table-bordered">
                    <thead>
                    <tr>
                        <th>T/R</th>
                        <th>Name</th>
                        <th colSpan="2">Action</th>
                    </tr>
                    </thead>

                    <tbody>
                    {this.state.countries?.map((country, i) =>
                        <tr key={i}>
                            <td>{i + 1}</td>
                            <td>{country.name}</td>
                            <td>
                                <button className="btn btn-primary"
                                        onClick={()=>this.editCountry(country)}>Edit
                                </button>
                            </td>
                            <td>
                                <button className="btn btn-danger"
                                        onClick={()=>this.deleteModal(country)}>Delete
                                </button>
                            </td>
                        </tr>
                    )}
                    </tbody>
                </table>
                <Modal isOpen={this.state.showModal}>
                    <ModalHeader>
                        {this.state.currentCountry ? 'Edit Country' : 'Add Country'}
                    </ModalHeader>

                    <ModalBody>
                        <AvForm onValidSubmit={this.saveCountry}>
                            <AvField
                                defaultValue={this.state.currentCountry.name}
                                name="name"/>
                            <button className="btn btn-success">Save</button>
                            <button type={"button"}
                                    onClick={this.closeModal}
                                    className="btn btn-danger">Cancel
                            </button>
                        </AvForm>
                    </ModalBody>
                </Modal>
                <Modal isOpen={this.state.showModalDelete}>
                    <ModalHeader>
                        {"Do you want really delete this Country " + this.state.currentCountry.name}
                    </ModalHeader>
                    <ModalFooter>
                        <button className="btn btn-info"
                                onClick={this.hideDeleteModal}>Cancel
                        </button>
                        <button className="btn btn-danger"
                                onClick={this.deleteCountry}>Delete
                        </button>
                    </ModalFooter>
                </Modal>
            </div>


        );
    }
}

Country.propTypes = {};

export default Country;