import React, {Component} from 'react';
import {Modal, ModalBody, ModalFooter, ModalHeader} from "reactstrap/es";
import {AvField, AvForm} from 'availity-reactstrap-validation';
import api from "../utils/api";
import request from "../utils/request";

class Student extends Component {
    state = {
        students: [],
        regions:[{
            districts: [],
        }],
        currentStudent: '',
        showModal: false,
        showModalDelete: false,
    }
    routeToGoHome = () => {
        this.props.history.push('/')
    }
    goBack=()=>{
        this.props.history.goBack();
    }

    openModal = () => {
        this.setState({showModal: true})
    }

    closeModal = () => {
        this.setState({showModal: false})
    }

    editStudent = (student) => {
        this.openModal();
        this.setState({currentStudent: student})
        console.log(student)
    }

    deleteModal = (student) => {
        this.setState({
            showModalDelete: true,
            currentStudent: student
        })
    }

    hideDeleteStudent = () => {
        this.setState({
            showModalDelete: false,
            currentStudent: ''
        })
    }

    componentDidMount() {
        this.getDistrict();
        this.getStudents();
        this.getRegion();
    }

    getDistrict=()=>{
        request({
            url:api.getDistrict,
            method:'GET'
        }).then(ans=>{
            this.setState({districts:ans.data})
            console.log(ans.data)
        }).catch(err=>{
            console.log(err)
        })
    }

    getRegion=()=>{
        request({
            url:api.getRegions,
            method:'GET'
        }).then(ans=>{
            this.setState({regions:ans.data})
            console.log(ans.data)
        }).catch(err=>{

        })
    }

    getStudents = () => {
        request({
            url: api.getStudents,
            method: 'GET'
        }).then(jovob => {
                this.setState({students: jovob.data})
                console.log(jovob.data)
            }).catch(err => {
            console.log(err)
        })
        console.log(api.getStudents)
    }

    saveStudent = (e, v) => {
        console.log(v)
        let current = this.state.currentStudent;
        console.log(current)
        request({
            url: current ? (api.editStudents + "/"+ current.id) : api.addStudents + '',
            method: current ? 'PUT' : 'POST',
            data: v
        }).then(res => {
            console.log(res)
            console.log('student/getAllStudents')
            // toast.info('Saved')
            // alert(current ? "Student edited" : "Student saved")
            this.getStudents();
            this.closeModal();
        }).catch(err => {
            console.log(err)
        })
    }

    deleteStudent = () => {
        let current = this.state.currentStudent;
        request({
            url: api.deleteStudents + '/' + current.id,
            method: 'DELETE',
        }).then(ans => {
            // alert("Student Deleted")
            // toast.success('Deleted')

            this.hideDeleteStudent();
            this.getStudents();
            console.log(ans)
        }).catch(err => {
            console.log(err)
        })
    }


    render() {
        return (
            <div>
                <h1 className="text-center">Student List</h1>
                <button className="btn btn-primary btn-outline-warning "
                        onClick={this.openModal}>+Add Student
                </button>
                <hr/>
                <table className="table table-bordered text-center">
                    <thead>
                    <tr className="text-info">
                        <th>t/r</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Birth Date</th>
                        {/*<th>Address</th>*/}
                        <th>Street</th>
                        <th colSpan={2}>Action</th>
                    </tr>
                    </thead>

                    <tbody>
                    {this.state.students?.map((students, i) =>
                        <tr key={i}>
                            <td>{i + 1}</td>
                            <td className="text-danger">{students.firstName}</td>
                            <td className="text-danger">{students.lastName}</td>
                            <td className="text-danger">{students.birthDate}</td>
                            {/*<td className="text-danger">{students.district}</td>*/}
                            <td className="text-danger">{students.street}</td>
                            <td>
                                <button
                                    className="btn btn-info"
                                    onClick={() => this.editStudent(students)}>Edit
                                </button>

                            </td>
                            <td>
                                <button className="btn btn-danger"
                                        onClick={() => this.deleteModal(students)}>Delete
                                </button>
                            </td>
                        </tr>
                    )}
                    </tbody>

                </table>
                <Modal isOpen={this.state.showModal}>
                    <ModalHeader>
                        {this.state.currentStudent ? 'Edit Student' : 'Add Student'}
                    </ModalHeader>

                    <ModalBody>
                        <AvForm onValidSubmit={this.saveStudent}>

                            <AvField
                                defaultValue={this.state.currentStudent.firstName}
                                name="firstName"
                                placeholder="Enter firstName"/>
                            <AvField
                                defaultValue={this.state.currentStudent.lastName}
                                name="lastName"
                                placeholder="Enter lastName"/>
                            <AvField
                                type="date"
                                name="birthDate">
                                <option value="">Select Birth Date</option>
                            </AvField>
                            <AvField
                            type="select"
                            name="region">
                                <option value="">Select region</option>
                                {this.state.regions?.map(region=>
                                    <option value={region.id}>{region.name}</option>)}
                            </AvField>
                            <AvField
                                type="select"
                                name="districtId">
                                <option value="">Select district</option>
                                {this.state.districts?.map(district =>
                                    <option value={district.id}>{district.name}</option>)}
                            </AvField>
                            <AvField
                            defaultValue={this.state.currentStudent.street}
                            name="street"
                            placeholder="Enter your address"/>
                            <button className="btn btn-success">Save</button>
                            <button type="button"
                                    onClick={this.closeModal}
                                    className="btn btn-danger">Cancel
                            </button>
                        </AvForm>
                    </ModalBody>
                </Modal>
                <Modal isOpen={this.state.showModalDelete}>
                    <ModalHeader>
                        {'Do you want delete this student:' + this.state.currentStudent.firstName}
                    </ModalHeader>
                    <ModalFooter>
                        <button onClick={this.deleteStudent}
                                className="btn btn-danger">Delete
                        </button>
                        <button className="btn btn-success"
                                onClick={this.hideDeleteStudent}>Cancel
                        </button>
                    </ModalFooter>
                </Modal>



                <button className="btn btn-primary pt-1"
                onClick={this.goBack}
                >&lt;go back</button>
                <button className="btn btn-info pt-1"
                onClick={this.routeToGoHome}>go Home</button>
            </div>


        );
    }
}

Student.propTypes = {};

export default Student;