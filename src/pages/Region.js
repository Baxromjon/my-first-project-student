import React, {Component} from 'react';
import PropTypes from 'prop-types';
import Modal from "reactstrap/es/Modal";
import {ModalBody, ModalFooter, ModalHeader} from "reactstrap/es";
import {AvField, AvForm} from 'availity-reactstrap-validation';
import request from "../utils/request";
import api from "../utils/api";

class Region extends Component {
    state = {
        countries: [],
        regions: [],
        currentRegion: '',
        showModal: false,
        showModalDelete:false
    }
    getCountries = () => {
        request({
            url: api.getCountries,
            method: 'GET'
        }).then(jovob => {
            this.setState({countries: jovob.data._embedded.list})
            console.log(jovob.data._embedded.list)
        }).catch(err => {
            // console.log(err.response.data)
        })
    }
    openModal = () => {
        this.setState({showModal: true})
    }
    closeModal = () => {
        this.setState({showModal: false})
    }

    componentDidMount() {
        this.getCountries();
        this.getRegion();
    }

    saveRegion = (e, v) => {
        let current = this.state.currentRegion;
        request(
            {
                url: api.addRegion,
                method: 'POST',
                data: v
            }).then(ans => {
            this.getRegion(),
            this.closeModal()
        }).catch(err => {
        })
    }
    getRegion = () => {
        request({
            url: api.getRegions,
            method: 'GET'
        }).then(ans => {
            this.setState({regions: ans.data})

        }).catch(err => {
        })
    }
    editRegion = (region) => {
        this.openModal();
        this.setState({currentRegion: region})
    }
    deleteModal = (region) => {
        this.setState({
            showModalDelete: true,
            currentRegion: region
        })
    }
    hideDeleteModal = () => {
        this.setState({
            showModalDelete:false,
            currentRegion:''
        })

    }
    deleteRegion = () => {
        request({
            url: api.deleteRegion + '/' + this.state.currentRegion.id,
            method: 'DELETE',
        }).then(ans => {
            this.hideDeleteModal();
            this.getRegion();
        }).catch(err => {
            // console.log(err.response.data)
        })
    }

    render() {
        return (
            <div>
                <h1 className="text-center">Region List</h1>
                <button className="btn btn-primary"
                        onClick={this.openModal}>+Add Region
                </button>
                <table className="table table-bordered">
                    <thead>
                    <tr>
                        <td>T/R</td>
                        <td>Region Name</td>
                        <td>Country Name</td>
                        <td>Action</td>
                    </tr>
                    </thead>

                    <tbody>
                    {this.state.regions?.map((regions, i) =>
                        (
                            <tr key={i}>
                                <td>{i + 1}</td>
                                <td>{regions.name}</td>
                                <td>{regions.country.name}</td>
                                <td>
                                    <button className="btn btn-primary"
                                    onClick={()=>this.editRegion(regions)}>Edit</button>
                                    <button className="btn btn-danger"
                                    onClick={()=>this.deleteModal(regions)}
                                    >Delete</button>
                                </td>
                            </tr>
                        )
                    )}

                    </tbody>
                </table>
                <Modal isOpen={this.state.showModal}>
                    <ModalHeader>
                        {this.state.currentRegion ? 'Edit Region' : 'Add Region'}
                    </ModalHeader>
                    <ModalBody>
                        <AvForm onValidSubmit={this.saveRegion}>
                            <AvField
                                defaultValue={this.state.currentRegion.name}
                                name="name"/>
                            <AvField
                                type="select"
                                name="countryId">
                                <option value="">select country</option>
                                {this.state.countries?.map(country =>
                                    <option value={country.id}>{country.name}</option>)}
                            </AvField>
                            <button className="btn btn-success mt-3">Save</button>
                            <button className="btn btn-danger mt-3"
                                    onClick={this.closeModal}>Cancel
                            </button>
                        </AvForm>
                    </ModalBody>
                </Modal>

                <Modal isOpen={this.state.showModalDelete}>
                    <ModalHeader>
                        {'Do you want delete this region '+this.state.currentRegion.name}
                    </ModalHeader>
                    <ModalFooter>
                        <button
                        className="btn btn-danger"
                        onClick={this.deleteRegion}>Delete</button>
                        <button
                        className="btn btn-success"
                        onClick={this.hideDeleteModal}>Cancel</button>
                    </ModalFooter>
                </Modal>
            </div>
        );
    }
}

Region.propTypes = {};

export default Region;