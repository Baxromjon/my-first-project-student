import React, {Component} from 'react';
import {LANGUAGE} from "../utils/constant";

class Home extends Component {
    getLang(e) {
        localStorage.setItem(LANGUAGE, e.target.value)
    }

    routeToRegion = () => {
        this.props.history.push('/region')
    }
    routeToCountry = () => {
        this.props.history.push('/country')
    }
    routeToDistrict = () => {
        this.props.history.push('/district')
    }
    routeToStudent = () => {
        this.props.history.push('/student')
    }

    render() {
        return (
            <div>
                <h1 className="text-center text-danger">Home page</h1>
                <button onClick={this.routeToCountry}
                        className="btn btn-info btn-outline-secondary m-1">Country List
                </button>
                <button onClick={this.routeToRegion}
                        className="btn btn-primary  btn-outline-danger m-1">Region List
                </button>
                <button onClick={this.routeToDistrict}
                        className="btn btn-dark m-1 btn-outline-info">District List
                </button>
                <button onClick={this.routeToStudent}
                        className="btn btn-success btn-outline-warning m-1">Student List
                </button>


            </div>
        );
    }
}

Home.propTypes = {};

export default Home;