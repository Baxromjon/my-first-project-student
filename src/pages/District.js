import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Modal, ModalBody, ModalFooter, ModalHeader} from "reactstrap/es";
import {AvField, AvForm} from 'availity-reactstrap-validation';
import request from "../utils/request";
import api from "../utils/api";

class District extends Component {
    state = {
        districts: [],
        regions: [],
        currentCountry: '',
        currentDistrict: '',
        countries: [],
        currentRegion: '',
        showModal: false,
    }

    componentDidMount() {
        this.getRegions();
        this.getDistrict();
        this.getCourse();
    }

    openModal = () => {
        this.setState({showModal: true})
    }
    closeModal = () => {
        this.setState({showModal: false})
    }

    getRegions = () => {
        request({
            url: api.getRegions,
            method: 'GET'
        }).then(ans => {
            this.setState({regions: ans.data})
            console.log(ans.data)
        }).catch(err => {
            console.log(err)
        })
    }
    getCourse = () => {
        request({
            url: api.getCountries,
            method: 'GET'
        }).then(res => {
            this.setState({countries: res.data._embedded.list})
            console.log(res.data._embedded.list)
        }).catch(err => {
        })
    }
    getAllRegionOfCountry = () => {
        let current = this.state.currentCountry
        console.log(current)
        request({
            url: api.getAllDistrictOfRegion + '/' + current,
            method: 'GET'
        }).then(res => {
            this.setState({regions: res.data.object})
            console.log(res.data.object)
        }).catch(err => {
        })
    }
    getCountry = (country) => {
        this.setState({currentCountry: country.target.value})
    }
    getDistrict = () => {
        request({
            url: api.getDistrict,
            method: 'GET'
        }).then(ans => {
            console.log(ans.data)
            this.setState({districts: ans.data})
        }).catch(err => {
        })
    }

    saveDistrict = (e, v) => {
        let current = this.state.currentDistrict;
        request({
            url: current ? (api.editDistrict + '/' + current.id) : api.addDistrict,
            method: current ? 'PUT' : 'POST',
            data: v
        }).then(res => {
            this.getDistrict();
            this.closeModal();
        }).catch(err => {
        })
        console.log(v)
    }
    editDistrict = (district) => {
        this.openModal();
        this.setState({
            currentDistrict: district
        })
    }

    deleteModal = (district) => {
        this.setState({
            showModalDelete: true,
            currentDistrict: district
        })
    }
    hideDeleteModal = () => {
        this.setState({
            showModalDelete: false,
            currentDistrict: '',
        })
    }
    deleteDistrict = () => {
        let current = this.state.currentDistrict;
        request({
            url: api.deleteDistrict + '/' + current.id,
            method: 'DELETE',
        }).then(ans => {
            this.hideDeleteModal();
            this.getDistrict();
            console.log(ans)
        }).catch(err => {
            console.log(err)
        })
    }

    render() {
        const {regions, countries} = this.state;
        return (
            <div>
                <h1 className="text-center">District List</h1>
                <button className="btn btn-info btn btn-outline-warning"
                        onClick={this.openModal}>+add District
                </button>
                <table className="table table-bordered">
                    <thead>
                    <tr>
                        <td>T/R</td>
                        <td>District Name</td>
                        <td>Region Name</td>
                        <td colSpan={2}>Action</td>
                    </tr>
                    </thead>
                    <tbody>
                    {this.state.districts?.map((district, i) =>
                        <tr key={i}>
                            <td>{i + 1}</td>
                            <td>{district.name}</td>
                            <td>{district.region.name}</td>
                            <td>
                                <button className="btn btn-info"
                                        onClick={() => this.editDistrict(district)}>Edit
                                </button>
                            </td>
                            <td>
                                <button className="btn btn-danger"
                                        onClick={() => this.deleteModal(district)}
                                >Delete
                                </button>
                            </td>

                        </tr>
                    )}
                    </tbody>
                </table>
                <Modal isOpen={this.state.showModal}>

                    <ModalHeader>
                        {this.state.currentDistrict?'Edit District':'Add District'}
                    </ModalHeader>
                    <ModalBody onValidSubmit={this.saveDistrict}>
                        <AvForm>
                            <AvField
                                defaultValue={this.state.currentDistrict.name}
                                name="name"/>
                            <AvField
                                type="select"
                                name="countryId"
                                onChange={(country) => this.getCountry(country)}
                                onClick={this.getAllRegionOfCountry}
                            >
                                <option value="">Select Country</option>
                                {countries.map(country =>
                                    <option value={country.id}>{country.name}</option>
                                )}
                            </AvField>
                            <AvField
                                type="select"
                                name="regionId">
                                <option value="">select region</option>
                                {regions.map(regions =>
                                    <option value={regions.id}>{regions.name}</option>
                                )}
                            </AvField>
                            <button className="btn btn-success">Save</button>
                            <button className="btn btn-danger"
                                    type={"button"}
                                    onClick={this.closeModal}>Cancel
                            </button>
                        </AvForm>
                    </ModalBody>
                </Modal>
                <Modal isOpen={this.state.showModalDelete}>
                    <ModalHeader>
                        {'Do you want delete this district: ' + this.state.currentDistrict.name}
                    </ModalHeader>
                    <ModalFooter>
                        <button onClick={this.deleteDistrict}
                                className="btn btn-danger">Delete
                        </button>
                        <button className="btn btn-success"
                                onClick={this.hideDeleteModal}>Cancel
                        </button>
                    </ModalFooter>
                </Modal>
            </div>
        );
    }
}

District.propTypes = {};

export default District;